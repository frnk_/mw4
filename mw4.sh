#!/usr/bin/env bash

readonly SALIDA="${HOME}/Descargas"
readonly SALIDA_XML='/tmp/feed.xml'
readonly PS3='Elije una opción: '

TITULO='MW4_Podcast'
NUM_DESCARGAS=''
URL_FEED=''
FORMATO='mp3|m4a|ogg|flac|acc|wav|wma'

# Comprueba si el archivo que contiene los feeds es accesible y lo carga
if [[ -f ./feed.list ]]; then
    # shellcheck disable=SC1091
    source ./feed.list
else
    echo "La lista de feeds no se encuentra disponible." >&2
    exit 1
fi

ayuda() {
    cat <<EOF
Modo de empleo: $0 [OPCION]... 
Descarga los audios correspondientes al podcast indicado.
Si no se indica un número límite de descargas (-L), se 
descargarán todos los audios existentes en el feed.
Sin opciones inícia en modo interactivo.

Opciones:
  -h          Muestra este mensaje de ayuda.
  -l          Lista los podcast disponibles.
  -p TITULO   Título del podcast de la lista a descargar.
  -f FORMATO  Formato de audio a descargar. Por omisión, busca los
              siguientes formatos mp3, m4a, ogg, flac, acc, wav y wma.
  -L NUM      Descarga últimos NUN del podcast.
  -u URL      Url del feed.
EOF
}

listar_feeds() {
    local podcasts

    if [[ -z "${PODCAST[*]}" ]]; then
        echo "El archivo de feeds se encuentra vacio." >&2
        exit 2
    else
        echo "Podcasts disponibles..."
        for podcasts in "${!PODCAST[@]}"; do
            echo "  $podcasts"
        done
    fi
}

menu_interactivo() {
    local opts

    titulo_y_limite() {
        read -rp "Dime el título del podcast: " TITULO
        # Convierte a minúsculas para evitar errores al pasar a la lista de feeds
        TITULO="${TITULO,,}"

        # Límite de descargas (sin límite por defecto)
        echo "Límitar el número de descargas?"
        read -r -p "Pulsa Intro (sin límite) o introduce un número: " NUM_DESCARGAS
    }

    # Lista los podcast disponibles
    listar_feeds

    echo -e "\nDescargar un podcast de la lista o introducir el feed manualmente?"

    select opts in lista manual; do
        case "$opts" in
            lista)
                titulo_y_limite
                break
                ;;
            manual)
                titulo_y_limite
                # Url del feed a descargar
                read -rp "Introduce la URL del feed a descargar: " URL_FEED                
                break
        esac
    done
}

opciones() {
    local opts
    
    while getopts ":hlp:f:L:u:" opts; do
        case "$opts" in
            h)  ayuda
                exit 0
                ;;
            l)  listar_feeds
                exit 0
                ;;
            p)  TITULO="${OPTARG,,}"
                ;;
            f)  FORMATO="${OPTARG,,}"
                ;;
            L)  NUM_DESCARGAS="$OPTARG"
                ;;
            u)  URL_FEED="$OPTARG"
                ;;
            \?) echo "La opción -$OPTARG no es válida." >&2
                ayuda
                exit 3
                ;;
        esac
    done

    shift $((OPTIND - 1))
}

creacion_directorio() {
    local salida="${SALIDA}/${TITULO}"

    echo "El título del Podcast es: " "$TITULO"

    while [ -d "${salida}" ]; do
        echo "Ya existe un directorio con el nombre de $TITULO"
        read -rp "Introduce un nuevo título para el directorio: " TITULO
        # Vacía el valor a `salida` para poder reasignarlo correctamente
        unset salida
        salida="${SALIDA}/${TITULO}"
    done

    mkdir "$salida" 2>/dev/null
    if [ -d "$salida" ]; then
        # Si no puede acceder al directorio, finaliza
        cd "$salida" || exit 4
    fi
}

descarga_xml() {
    # Si el título se encuentra en la lista de feeds, toma este como valor
    if [[ -n "${PODCAST[$TITULO]}" ]]; then
        URL_FEED="${PODCAST[$TITULO]}"
    fi

    # Comprueba que exista que $URL_FEED no esté vacía
    if [[ -z "$URL_FEED" ]]; then
        echo "Debes indicar un título válido o una url." >&2
        exit 5
    fi

    # descarga el feed en un archivo llamado archivo.xml, lo sobreescribe si existe
    wget --quiet --output-document=/tmp/feed.xml "${URL_FEED}" 

    # Finaliza si no ha finalizado wget correctamente
    # shellcheck disable=SC2181
    [[ "$?" -ne 0 ]] && \
        echo "Error en la descarga del archivo XML!" >&2 && exit 6
}

descarga_podcast() {
    local audio_podcast
    local n

    audio_podcast=(
        $(grep -i 'enclosure url=' "$SALIDA_XML" \
            | grep -o -E "http[[:graph:]]+\.($FORMATO)")
    )

    echo "Descargando podcasts..."
    # Si es nulo descarga todos los audios del podcast
    if [[ -z "$NUM_DESCARGAS" ]]; then
        # --no-verbose 
        wget --quiet --show-progress "${audio_podcast[@]}"
    else
        # Descarga el número de podcasts indicado
        for (( n=0; n < "$NUM_DESCARGAS"; n++ )); do
            wget --quiet --show-progress "${audio_podcast[n]}"
        done
    fi

    rm "$SALIDA_XML"
}

check_descargas() {
    # Obtiene el número de archivos descargados
    local archivos
    archivos=$(find . -iname '*\.???' | wc -l)

    if [[ "$archivos" -eq 0 ]]; then
        echo "No se ha podido descargar ningún archivo de audio." >&2
        rm "${SALIDA}/${TITULO}"
        exit 7
    else
        echo "Archivos descargados: $archivos"
        echo "Descargas finalizadas con éxito."
    fi
}

# MAIN

# Sin opciones muestra menú interactivo
if [[ $# == 0 ]]; then
    menu_interactivo
else
    opciones "$@"

    echo "Título: $TITULO"
    echo "Número de descargas: $NUM_DESCARGAS"
    # Si el valor de URL_FEED es nulo (no se introdujo de forma manual),
    # toma el valor de PODCAST[TITULO] (seleccionado de la lista de feeds)
    echo -e "Url del feed: ${URL_FEED:-${PODCAST[$TITULO]}}\n"
fi

descarga_xml
creacion_directorio
descarga_podcast
check_descargas
